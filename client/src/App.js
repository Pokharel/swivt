import react,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import {Alert,Button,ButtonGroup,Col,Container,Row,Form,FormControl} from 'react-bootstrap';
import SearchResult from './components/search/searchResults';

function App() {
  const [result,setResult] = useState([]);
  const [searchText,setSearchText] = useState();
  const [perPage,setPerPage] = useState(10);
  const [sortBy,setSortBy] = useState();
  const [loading,setLoading] = useState(false);
  const searchApi = '/search/';

  useEffect(() => {    
    setResult([]);
    if(searchText && searchText.length > 3){
     handleSearch();    
    }
  },[searchText,perPage,sortBy]);

  const handleSearch = async(options) =>{      
      setLoading(true);
      let querystring = searchApi+searchText+'?perPage='+perPage;
      //default sorted by best match
      if(sortBy){
        querystring +='&sortBy='+sortBy;
      }

      //fetch results for github api
      fetch(querystring)
      .then(response => response.json())
      .then((res) =>{
        console.log('res',res);
          setResult(JSON.parse(res.data));
          setLoading(false);
        }
      );
    }
  
  return (
    <Container>
   
      <Form className="mt-2">
        <Form.Group className="mb-3" controlId="searchForm.searchRepo">
         <Row>
        <Col sm={7} className="my-1">
          <Form.Control size="lg" onChange={(e) => setSearchText(e.target.value)} type="text" placeholder="search repo" />
        </Col>        
        {searchText &&
          <>
            <Col sm={2} className="my-1">
              <Form.Select onChange={(e) => setPerPage(e.target.value)} aria-label="Default select example">
                <option>Items per page</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
               </Form.Select>
            </Col>
            <Col sm={3} className="my-1">
            <ButtonGroup aria-label="Sort options">
              <Button variant="secondary" onClick={(e) => setSortBy('stars')}>Stars</Button>
              <Button variant="secondary" onClick={(e) => setSortBy('forks')}>Forks</Button>
              <Button variant="secondary" onClick={(e) => setSortBy('updated')}>Updated</Button>
            </ButtonGroup>
            </Col>            
          </>
        }
        </Row>
        </Form.Group>
      </Form>
   
    {loading &&
          <Alert variant={'success'}>
           {'Please wait ...'}
          </Alert>    
        }
    {searchText && result.total_count == 0 &&
      <Alert variant={'warning'}>
        No repositories were matched.
      </Alert>
    }
    <SearchResult data={result}/>
  </Container>
  );
}

export default App;
