import react,{useEffect,useState} from 'react';
import {ListGroup,Button,Row,Col,Card} from 'react-bootstrap';

const SearchResults = (props) => {
 const [detail,setDetail] = useState();
  return (
      <Row>
      {!detail && props.data && props.data.items && props.data.items.map((d,k) => 
        <Col key={k} sm={3} className="my-1">
         <Card style={{ width: '18rem' }}>
          <Card.Body>
            <Card.Title>{d.name}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{'Author :' + d.owner.login}</Card.Subtitle>
            <Card.Subtitle className="mb-2 text-muted">{'Forks :' + d.forks_count}</Card.Subtitle>
            <Card.Subtitle className="mb-2 text-muted">{'Watchers :' + d.watchers_count}</Card.Subtitle>
            <Card.Subtitle className="mb-2 text-muted">{'Last updated date :' + d.updated_at}</Card.Subtitle>
            <Card.Text>{d.description}
            </Card.Text>
            <Button variant="primary" onClick={(e) => setDetail(d)}>View Detail</Button>
          </Card.Body>
        </Card>   
        </Col>
        )   
        } 
        {detail &&
          <>
          <ListGroup>
            <ListGroup.Item>{'Repository Name: '+detail.full_name}</ListGroup.Item>
            <ListGroup.Item>{'Author: '+detail.owner.login}</ListGroup.Item>
            <ListGroup.Item>{'Default Branch: '+detail.default_branch}</ListGroup.Item>
            <ListGroup.Item>{'Open issues count: '+detail.open_issues_count}</ListGroup.Item>
          </ListGroup>
          <Col sm={3} className="my-1">
            <Button variant="primary" onClick={(e) => setDetail()}>Back</Button>
          </Col>
          </>
        }
      </Row>
    )
}
export default SearchResults;