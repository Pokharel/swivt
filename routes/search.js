var express = require('express');
var router = express.Router();
const https = require('https');


/* GET users listing. */
router.get('/:query', function(req, res, next) {
  let {query} = req.params;
  let {perPage,sortBy} = req.query;
  let querystring = '/search/repositories?q='+query
  if(perPage){
    querystring +='&per_page='+perPage;
  }
  if(sortBy){
    querystring +='&sort='+sortBy;
  }
  var options = {
    host: 'api.github.com',
    path: querystring,
    method: 'GET',
    headers: {'user-agent': 'node.js'}
  };
  https.get(options, resp => {
    let data = '';

    // A chunk of data has been received.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      res.send({data});
    });
  }).on("error", (err) => {
      console.log("Error: " + err.message);
    });
});

module.exports = router;
